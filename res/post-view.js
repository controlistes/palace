function format_posts() {
    const images = document.querySelectorAll('figure img');

    images.forEach(image => {
        const imageHeight = image.offsetHeight;
        const imageWidth = image.offsetWidth;

        if (imageHeight < imageWidth) {
            image.closest(".post").classList.add('flex');
        }
    });
}


if (document.readyState === 'interactive' || document.readyState === 'complete')
    window.setTimeout(format_posts, 0);
else
    document.addEventListener('DOMContentLoaded', format_posts);