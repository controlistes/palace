use async_trait::async_trait;
use axum_login::{AuthUser, AuthnBackend, AuthzBackend, UserId};
use password_auth::verify_password;
use rusqlite::{
    types::{FromSql, FromSqlResult, ToSqlOutput, ValueRef},
    Connection, ToSql,
};
use serde::{Deserialize, Serialize};
use std::hash::{Hash, Hasher};
use std::{
    fmt::{Display, Formatter},
    sync::Arc,
};
use tokio::{sync::Mutex, task};

use crate::database::{get_user_by_id, get_user_by_name};

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum LoginError {
    Internal,
    Unauthorized,
}

impl Display for LoginError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let msg = match self {
            Self::Internal => "Internal server error",
            Self::Unauthorized => "Forbidden page",
        };

        write!(f, "{}", msg)
    }
}

#[derive(Clone, Debug)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub password: String,
    pub level: StaffPerms,
}

#[derive(Clone, Debug, Serialize, Deserialize, Eq)]
pub enum StaffPerms {
    Admin,
    Janitor,
    Unpriviledged,
}

impl Display for StaffPerms {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let msg = match self {
            Self::Admin => "Admin",
            Self::Janitor => "Janitor",
            Self::Unpriviledged => "Unpriviledged",
        };

        write!(f, "{}", msg)
    }
}

impl PartialEq for StaffPerms {
    fn eq(&self, other: &Self) -> bool {
        use StaffPerms::*;
        match (self, other) {
            (Admin, Admin) => true,
            (Janitor, Janitor) => true,
            _ => false,
        }
    }
}

impl Hash for StaffPerms {
    fn hash<H: Hasher>(&self, state: &mut H) {
        use StaffPerms::*;
        match self {
            Admin => {
                state.write_u8(0);
            }
            Janitor => {
                state.write_u8(1);
            }
            _ => {
                state.write_u8(8);
            }
        }
    }
}

impl FromSql for StaffPerms {
    fn column_result(value: ValueRef) -> FromSqlResult<Self> {
        i64::column_result(value).and_then(|as_i64| match as_i64 {
            0 => Ok(Self::Admin),
            1 => Ok(Self::Janitor),
            _ => Ok(Self::Unpriviledged),
        })
    }
}

impl ToSql for StaffPerms {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        match &self {
            Self::Admin => Ok(ToSqlOutput::from(0)),
            Self::Janitor => Ok(ToSqlOutput::from(1)),
            Self::Unpriviledged => Ok(ToSqlOutput::from(99)),
        }
    }
}

impl AuthUser for User {
    type Id = i32;

    fn id(&self) -> Self::Id {
        self.id
    }

    fn session_auth_hash(&self) -> &[u8] {
        &self.password.as_bytes()
    }
}

#[derive(Clone)]
pub struct Backend {
    conn: Arc<Mutex<Connection>>,
}

impl Backend {
    pub fn new(conn: Arc<Mutex<Connection>>) -> Self {
        Self { conn }
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct Credentials {
    pub username: String,
    pub password: String,
}

#[derive(Debug, thiserror::Error)]
pub enum AuthError {
    #[error(transparent)]
    Rusqlite(#[from] rusqlite::Error),

    #[error(transparent)]
    TaskJoin(#[from] task::JoinError),
}

#[async_trait]
impl AuthnBackend for Backend {
    type User = User;
    type Credentials = Credentials;
    type Error = AuthError;

    async fn get_user(&self, user_id: &UserId<Self>) -> Result<Option<Self::User>, Self::Error> {
        let conn = self.conn.lock().await;
        let user = get_user_by_id(&conn, user_id.to_owned() as i32)?;
        Ok(user)
    }

    async fn authenticate(
        &self,
        creds: Self::Credentials,
    ) -> Result<Option<Self::User>, Self::Error> {
        let conn = self.conn.lock().await;
        let user = get_user_by_name(&conn, creds.username.as_str())?;
        task::spawn_blocking(|| {
            Ok(user.filter(|user| verify_password(creds.password, &user.password).is_ok()))
        })
        .await?
    }
}

#[async_trait]
impl AuthzBackend for Backend {
    type Permission = StaffPerms;

    async fn has_perm(
        &self,
        user: &Self::User,
        perm: Self::Permission,
    ) -> Result<bool, Self::Error> {
        Ok(user.level == perm)
    }
}
