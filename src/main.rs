#![recursion_limit = "512"]

use auth::{Backend, StaffPerms};
use axum::{
    body::Body,
    extract::{ConnectInfo, DefaultBodyLimit, Path},
    http::{HeaderMap, Response, StatusCode},
    response::{Html as ResponseHtml, IntoResponse, Redirect},
    routing::{get, post},
    Extension, Form, Router,
};
use axum_login::{login_required, permission_required, AuthManagerLayerBuilder};
use axum_typed_multipart::{FieldData, TryFromMultipart, TypedMultipart};
use bytes::Bytes;
use components::{
    admin_page, error_page, login_page, main_page, post_page, read_file_to_string, static_page,
};
use database::{
    create_ban, create_user, delete_ban, delete_post_by_id, delete_posts_by_author, delete_user,
    get_main_threads, get_post_by_id, get_posts_in_thread, get_userbans, get_users, init_db,
    is_banned,
};
use rusqlite::Connection;
use serde::Deserialize;
use std::{fs, io, net::SocketAddr, sync::Arc};
use thiserror::Error;
use tokio::sync::Mutex;
use tower_http::services::ServeDir;
use tower_sessions::{cookie::time::Duration, Expiry, MemoryStore, SessionManagerLayer};

use crate::auth::Credentials;
use crate::database::create_post;

mod auth;
mod components;
mod database;
mod parser;

type AuthSession = axum_login::AuthSession<Backend>;

#[tokio::main]
async fn main() {

    let upload_path = "uploads";

    // Software should panic if it can't create the upload folder
    create_directory_if_not_exists(upload_path).unwrap();

    let db_conn = Arc::new(Mutex::new(init_db().unwrap()));

    {
        let conn = db_conn.lock().await;
        database::populate_sample_data(&conn).unwrap();
    }

    let session_store = MemoryStore::default();
    let session_layer = SessionManagerLayer::new(session_store)
        .with_expiry(Expiry::OnInactivity(Duration::days(1)));

    let backend = Backend::new(db_conn.clone());
    let auth_layer = AuthManagerLayerBuilder::new(backend, session_layer).build();

    let app = Router::new()
        // Admin management
        .route("/admin", get(admin_view))
        .route("/admin/ban/delete", post(post_admin_ban_delete_handler))
        .route("/admin/staff/create", post(post_admin_staff_create_handler))
        .route("/admin/staff/delete", post(post_admin_staff_delete_handler))
        .route_layer(permission_required!(
            Backend,
            login_url = "/login",
            StaffPerms::Admin
        ))
        // Posts management
        .route("/post/mod/delete", post(post_posts_mod_delete_handler))
        .route("/post/mod/purge", post(post_posts_mod_purge_handler))
        .route("/post/mod/ban", post(post_posts_mod_ban_handler))
        .route_layer(login_required!(Backend, login_url = "/login"))
        // Main views
        .route("/", get(main_view).post(post_main_handler))
        .layer(DefaultBodyLimit::max(1024 * 1024 * 10))
        .route("/posts/:post_id", get(post_view).post(post_posts_handler))
        .layer(DefaultBodyLimit::max(1024 * 1024 * 10))
        .route("/login", get(login_view).post(post_login_handler))
        .route("/logout", post(post_logout_handler))
        .route("/about", get(about_view))
        .route("/links", get(links_view))
        .route("/conduct", get(conduct_view))
        // Services
        .layer(auth_layer)
        .layer(Extension(db_conn))
        .fallback(fallback_handler)
        // Static files
        .nest_service("/res", ServeDir::new("res"))
        .nest_service("/uploads", ServeDir::new("uploads"));

    let listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await.unwrap();
    axum::serve(
        listener,
        app.into_make_service_with_connect_info::<SocketAddr>(),
    )
    .await
    .unwrap();
}

async fn admin_view(Extension(db_conn): Extension<Arc<Mutex<Connection>>>) -> impl IntoResponse {
    let db_conn = db_conn.lock().await;
    let bans = get_userbans(&db_conn).unwrap_or(vec![]);
    let staff = get_users(&db_conn).unwrap_or(vec![]);
    ResponseHtml(admin_page(bans, staff))
}

fn create_directory_if_not_exists(path: &str) -> io::Result<()> {
    if !fs::metadata(path).is_ok() {
        fs::create_dir_all(path)?;
    }
    Ok(())
}

#[derive(Deserialize)]
pub struct DeleteBanForm {
    pub ip: String,
}

async fn post_admin_ban_delete_handler(
    Extension(db_conn): Extension<Arc<Mutex<Connection>>>,
    Form(data): Form<DeleteBanForm>,
) -> Response<Body> {
    let db_conn = db_conn.lock().await;

    match delete_ban(&db_conn, data.ip) {
        Ok(_) => Redirect::to(format!("/admin").as_str()).into_response(),
        Err(_) => AppError::Internal.into_response(),
    }
}

#[derive(Deserialize)]
pub struct DeleteStaffForm {
    pub id: i32,
}

async fn post_admin_staff_delete_handler(
    Extension(db_conn): Extension<Arc<Mutex<Connection>>>,
    Form(data): Form<DeleteStaffForm>,
) -> Response<Body> {
    let db_conn = db_conn.lock().await;

    match delete_user(&db_conn, data.id) {
        Ok(_) => Redirect::to(format!("/admin").as_str()).into_response(),
        Err(_) => AppError::Internal.into_response(),
    }
}

async fn main_view(Extension(db_conn): Extension<Arc<Mutex<Connection>>>) -> impl IntoResponse {
    let db_conn = db_conn.lock().await;
    let threads = get_main_threads(&db_conn).unwrap();
    ResponseHtml(main_page(&db_conn, threads.iter()))
}

async fn post_view(
    Extension(db_conn): Extension<Arc<Mutex<Connection>>>,
    Path(post_id): Path<i32>,
    auth_session: AuthSession,
) -> impl IntoResponse {
    let priv_level = match auth_session.user {
        Some(user) => user.level,
        None => StaffPerms::Unpriviledged,
    };
    let db_conn = db_conn.lock().await;
    let post = get_post_by_id(&db_conn, post_id).unwrap_or(None);
    let replys = get_posts_in_thread(&db_conn, post_id).unwrap_or(vec![]);
    ResponseHtml(post_page(post, replys.iter(), &db_conn, priv_level))
}

async fn about_view() -> impl IntoResponse {
    let content = read_file_to_string("static/about.html");
    ResponseHtml(static_page(
        content.unwrap_or("Cette page n'existe pas".to_owned()),
    ))
}

async fn links_view() -> impl IntoResponse {
    let content = read_file_to_string("static/links.html");
    ResponseHtml(static_page(
        content.unwrap_or("Cette page n'existe pas".to_owned()),
    ))
}

async fn conduct_view() -> impl IntoResponse {
    let content = read_file_to_string("static/conduct.html");
    ResponseHtml(static_page(
        content.unwrap_or("Cette page n'existe pas".to_owned()),
    ))
}

async fn login_view(auth_session: AuthSession) -> impl IntoResponse {
    let user = auth_session.user;
    ResponseHtml(login_page(user))
}

async fn post_login_handler(
    mut auth_session: AuthSession,
    Form(creds): Form<Credentials>,
) -> Response<Body> {
    let user = match auth_session.authenticate(creds.clone()).await {
        Ok(Some(user)) => user,
        Ok(None) => return AppError::Unauthorized.into_response(),
        Err(_) => return AppError::Internal.into_response(),
    };

    if auth_session.login(&user).await.is_err() {
        return AppError::Internal.into_response();
    }

    Redirect::to("/login").into_response()
}

async fn post_logout_handler(mut auth_session: AuthSession) -> Response<Body> {
    let _ = match &auth_session.user {
        Some(user) => user,
        None => return AppError::Unauthorized.into_response(),
    };

    let _ = auth_session.logout().await;

    Redirect::to("/login").into_response()
}

#[derive(Deserialize)]
pub struct DeleteForm {
    pub id: i32,
}

async fn post_posts_mod_delete_handler(
    Extension(db_conn): Extension<Arc<Mutex<Connection>>>,
    Form(data): Form<DeleteForm>,
) -> Response<Body> {
    let mut db_conn = db_conn.lock().await;

    let id: String = get_post_by_id(&db_conn, data.id)
        .ok()
        .flatten()
        .and_then(|post| post.parent)
        .map_or_else(|| "".to_owned(), |parent| parent.to_string());

    match delete_post_by_id(&mut db_conn, data.id) {
        Ok(_) => Redirect::to(format!(r#"/posts/{}"#, id).as_str()).into_response(),
        Err(_) => AppError::Internal.into_response(),
    }
}

#[derive(Deserialize)]
pub struct BanForm {
    pub id: i32,
}

async fn post_posts_mod_ban_handler(
    Extension(db_conn): Extension<Arc<Mutex<Connection>>>,
    Form(data): Form<BanForm>,
) -> Response<Body> {
    let db_conn = db_conn.lock().await;

    let post = match get_post_by_id(&db_conn, data.id) {
        Ok(Some(post)) => post,
        _ => return AppError::Internal.into_response(),
    };

    let parent = match post.parent {
        Some(id) => format!("{id}"),
        None => "".to_owned(),
    };

    if is_banned(&db_conn, &post.author) == Ok(true) {
        return AppError::BadRequest.into_response();
    }

    match create_ban(&db_conn, &post.author, None) {
        Ok(_) => Redirect::to(format!(r#"/posts/{}#{}"#, parent, post.id).as_str()).into_response(),
        Err(_) => AppError::Internal.into_response(),
    }
}

#[derive(Deserialize)]
pub struct CreateStaffForm {
    pub username: String,
    pub password: String,
    pub level: i64,
}

async fn post_admin_staff_create_handler(
    Extension(db_conn): Extension<Arc<Mutex<Connection>>>,
    Form(data): Form<CreateStaffForm>,
) -> Response<Body> {
    let db_conn = db_conn.lock().await;

    if data.username.trim().is_empty() == true || data.password.trim().is_empty() == true {
        return AppError::BadRequest.into_response();
    }

    match create_user(&db_conn, &data.username, &data.password, &data.level) {
        Ok(_) => Redirect::to("/admin").into_response(),
        Err(_) => AppError::Internal.into_response(),
    }
}

#[derive(Deserialize)]
pub struct PurgeForm {
    pub id: i32,
}

async fn post_posts_mod_purge_handler(
    Extension(db_conn): Extension<Arc<Mutex<Connection>>>,
    Form(data): Form<PurgeForm>,
) -> Response<Body> {
    let mut db_conn = db_conn.lock().await;

    let post = match get_post_by_id(&db_conn, data.id) {
        Ok(Some(post)) => post,
        _ => return AppError::Internal.into_response(),
    };

    let parent = post
        .parent
        .map_or_else(|| "".to_owned(), |id| format!("{}", id));

    if is_banned(&db_conn, &post.author) == Ok(true) {
        return AppError::BadRequest.into_response();
    }

    match delete_posts_by_author(&mut db_conn, &post.author)
        .and_then(|_| create_ban(&db_conn, &post.author, None))
    {
        Ok(_) => Redirect::to(format!(r#"/posts/{}#{}"#, parent, post.id).as_str()).into_response(),
        Err(_) => AppError::Internal.into_response(),
    }
}

#[derive(TryFromMultipart)]
struct CreatePostRequest {
    nick: String,
    message: String,
    #[form_data(limit = "8MiB")]
    upload: Option<FieldData<Bytes>>,
}

async fn post_main_handler(
    headers: HeaderMap,
    Extension(db_conn): Extension<Arc<Mutex<Connection>>>,
    ConnectInfo(addr): ConnectInfo<SocketAddr>,
    data: TypedMultipart<CreatePostRequest>,
    
) -> Response<Body> {
    let ip = addr.ip().to_string();
    let mut db_conn = db_conn.lock().await;

    let forwarded = headers.get("x-forwarded-for");
    let author = forwarded.map_or(ip.as_str(), |option| option.to_str().unwrap_or("ERROR"));
    
    println!("post created by {}", author);

    if is_banned(&db_conn, &ip).unwrap_or(false) {
        return AppError::Unauthorized.into_response();
    }

    match create_post(&mut db_conn, author, data, None) {
        Ok(created_id) => Redirect::to(format!("/posts/{}", created_id).as_str()).into_response(),
        Err(err) => err.into_response(),
    }
}

async fn post_posts_handler(
    headers: HeaderMap,
    Extension(db_conn): Extension<Arc<Mutex<Connection>>>,
    ConnectInfo(addr): ConnectInfo<SocketAddr>,
    Path(thread_id): Path<i32>,
    data: TypedMultipart<CreatePostRequest>,
) -> Response<Body> {
    let ip = addr.ip().to_string();
    let mut db_conn = db_conn.lock().await;

    let forwarded = headers.get("x-forwarded-for");
    let author = forwarded.map_or(ip.as_str(), |option| option.to_str().unwrap_or("ERROR"));
    
    println!("post created by {}", author);

    if is_banned(&db_conn, &ip).unwrap_or(false) {
        return AppError::Unauthorized.into_response();
    }

    match create_post(&mut db_conn, author, data, Some(thread_id)) {
        Ok(created_id) => {
            Redirect::to(format!("/posts/{thread_id}#{created_id}").as_str()).into_response()
        }
        Err(err) => err.into_response(),
    }
}

async fn fallback_handler() -> Response<Body> {
    AppError::NotFound.into_response()
}

#[derive(Error, Debug)]
pub enum AppError {
    #[error("Unauthorized")]
    Unauthorized,
    #[error("Not Found")]
    NotFound,
    #[error("Internal Server Error")]
    Internal,
    #[error("Bad Request")]
    BadRequest,
}

impl IntoResponse for AppError {
    fn into_response(self) -> Response<Body> {
        let (status, error_message) = match self {
            AppError::Unauthorized => (StatusCode::UNAUTHORIZED, "401 Unauthorized"),
            AppError::NotFound => (StatusCode::NOT_FOUND, "404 Not Found"),
            AppError::Internal => (
                StatusCode::INTERNAL_SERVER_ERROR,
                "500 Internal Server Error",
            ),
            AppError::BadRequest => (StatusCode::BAD_REQUEST, "400 Bad Request"),
        };

        let body = Body::new(error_page(error_message.as_ref()));

        Response::builder().status(status).body(body).unwrap()
    }
}

#[derive(Error, Debug)]
pub enum CreatePostError {
    #[error("Invalid Form")]
    InvalidForm,
    #[error("Invalid Upload Format")]
    InvalidUpload,
    #[error("Reply Limit Reached")]
    ReplyLimit,
    #[error("Missing Thread")]
    MissingThread,
    #[error("Internal Server Error")]
    Internal,
}

impl IntoResponse for CreatePostError {
    fn into_response(self) -> Response<Body> {
        let (status, error_message) = match self {
            CreatePostError::InvalidForm => (StatusCode::BAD_REQUEST, "400 Invalid form"),
            CreatePostError::InvalidUpload => {
                (StatusCode::BAD_REQUEST, "400 Invalid upload format")
            }
            CreatePostError::MissingThread => (StatusCode::NOT_FOUND, "404 Missing thread"),
            CreatePostError::ReplyLimit => (StatusCode::FORBIDDEN, "403 Reply limit reached"),
            CreatePostError::Internal => (
                StatusCode::INTERNAL_SERVER_ERROR,
                "500 Internal Server Error",
            ),
        };

        let body = Body::new(error_page(error_message.as_ref()));

        Response::builder().status(status).body(body).unwrap()
    }
}
