use std::fs::File;
use std::io::{self, BufRead, BufReader, Read};

use chrono::prelude::*;
use html::content::children::MainChild;
use html::content::{Article, Footer, Main};
use html::root::children::BodyChild;
use html::root::Html;
use html::text_content::{Division, Figure, ListItem, UnorderedList};
use html::{content::Header, forms::Form};
use htmlentity::entity::{encode, CharacterSet, EncodeType, ICodedDataTrait};
use palette::{FromColor, Hsl, Srgb};
use rand::Rng;
use rusqlite::Connection;
use sha2::{Digest, Sha256};

use crate::auth::{StaffPerms, User};
use crate::database::{get_mentions_by_target, get_post_by_id, get_posts_in_thread, UserBan};
use crate::parser::{format_bold, format_paragraph, format_quote};
use crate::{database::Post, parser::format_mentions, parser::format_urls};

pub fn main_page<'a, T: Iterator<Item = &'a Post>>(conn: &Connection, posts: T) -> String {
    let mut c: Vec<BodyChild> = Vec::new();

    c.push(
        Main::builder()
            .heading_1(|h1| h1.text("Discussions"))
            .push(thread_list(&conn, posts))
            .heading_2(|h2| h2.text("Nouvelle discussion"))
            .push(post_form("/".to_owned()))
            .build()
            .into(),
    );

    base_template(c).to_string()
}

pub fn static_page(html: String) -> String {
    let mut c: Vec<BodyChild> = Vec::new();

    c.push(Main::builder().text(html).build().into());

    base_template(c).to_string()
}

pub fn admin_page(bans: Vec<UserBan>, staff: Vec<User>) -> String {
    let mut c: Vec<BodyChild> = Vec::new();

    c.push(
        Main::builder()
            .heading_1(|h1| h1.text("Administration"))
            .heading_2(|h2| h2.text("Comptes"))
            .push(user_list(staff.iter()))
            .push(create_staff_form("/admin/staff/create".to_owned()))
            .heading_2(|h2| h2.text("Bannissements"))
            .push(ban_list(bans.iter().rev()))
            .build()
            .into(),
    );

    base_template(c).to_string()
}

fn ban_list<'a, T: Iterator<Item = &'a UserBan>>(bans: T) -> UnorderedList {
    let mut list = UnorderedList::builder();
    let mut bans = bans.peekable();
    if bans.peek() == None {
        return list
            .class("ban-list")
            .list_item(|li| li.text("Pas de bannissement en cours"))
            .build();
    }
    list.class("ban-list")
        .extend(bans.map(|data| ban(data)))
        .build()
}

fn ban(data: &UserBan) -> ListItem {
    let controls = Form::builder()
        .class("ban-form")
        .action("POST")
        .input(|input| {
            input
                .type_("hidden")
                .name("ip")
                .value(format!("{}", data.ip))
        })
        .input(|input| {
            input
                .type_("submit")
                .formaction("/admin/ban/delete")
                .formmethod("POST")
                .value("Supprimer")
        })
        .build();

    ListItem::builder()
        .class("ban")
        .span(|span| span.text(format!("{}", data.ip)))
        .push(controls)
        .build()
}

fn user_list<'a, T: Iterator<Item = &'a User>>(users: T) -> UnorderedList {
    UnorderedList::builder()
        .class("user-list")
        .extend(users.map(|data| user(data)))
        .build()
}

fn user(data: &User) -> ListItem {
    let controls = Form::builder()
        .class("user-form")
        .action("POST")
        .input(|input| {
            input
                .type_("hidden")
                .name("id")
                .value(format!("{}", data.id))
        })
        .input(|input| {
            input
                .type_("submit")
                .formaction("/admin/staff/delete")
                .formmethod("POST")
                .value("Supprimer")
        })
        .build();

    ListItem::builder()
        .class("user")
        .span(|span| span.text(format!("{}", data.username)))
        .span(|span| span.text(format!("{}", data.level)))
        .push(controls)
        .build()
}

fn create_staff_form(action: String) -> Form {
    Form::builder()
        .id("post")
        .action(action)
        .method("post")
        .class("create-staff-form")
        .input(|input| input.type_("text").name("username").placeholder("username"))
        .select(|select| {
            select
                .name("level")
                .option(|option| option.value("1").text("Janitor"))
                .option(|option| option.value("0").text("Admin"))
        })
        .input(|input| {
            input
                .type_("password")
                .name("password")
                .placeholder("password")
        })
        .input(|input| input.type_("submit").value("create"))
        .build()
}

pub fn post_page<'a, T: Iterator<Item = &'a Post>>(
    thread_post: Option<Post>,
    replys: T,
    conn: &Connection,
    priv_level: StaffPerms,
) -> String {
    let mut c: Vec<BodyChild> = Vec::new();

    let thread = match thread_post {
        Some(post) => post,
        None => {
            c.push(
                Main::builder()
                    .paragraph(|p| p.text("This thread doesn't exist"))
                    .build()
                    .into(),
            );
            return base_template(c).to_string();
        }
    };

    let body = encode(
        thread.body.as_bytes(),
        &EncodeType::NamedOrHex,
        &CharacterSet::Html,
    )
    .to_string()
    .unwrap();

    c.push(
        Main::builder()
            .class("post-view")
            .script(|script| script.src("/res/post-view.js"))
            .heading_1(|h1| h1.text(format!("{}", get_summary(body.as_str(), 64))))
            .push(post(&thread, conn, priv_level.clone()))
            .push(post_list(replys, conn, priv_level))
            .heading_2(|h2| h2.text("Répondre"))
            .push(post_form(format!("/posts/{}", &thread.id)))
            .build()
            .into(),
    );
    // c.push(Aside::builder().push(post_form(format!("/posts/{}", &thread.id))).build().into());

    base_template(c).to_string()
}

pub fn login_page(user: Option<User>) -> String {
    let mut c: Vec<BodyChild> = Vec::new();

    let content: MainChild = match user {
        Some(user) => MainChild::Division(
            Division::builder()
                .paragraph(|p| p.text(format!("Connecté en tant que <b>{}</b>", user.username)))
                .form(|form| {
                    form.method("post")
                        .action("/logout")
                        .input(|input| input.type_("submit").value("Se déconnecter"))
                })
                .build(),
        ),
        None => MainChild::Form(login_form("/login".to_owned())),
    };

    c.push(
        Main::builder()
            .heading_1(|h1| h1.text("Connexion"))
            .push(content)
            .build()
            .into(),
    );

    base_template(c).to_string()
}

pub fn error_page(error: &str) -> String {
    let mut c: Vec<BodyChild> = Vec::new();

    c.push(
        Main::builder()
            .paragraph(|p| p.class("error").text(error.to_owned()))
            .build()
            .into(),
    );

    base_template(c).to_string()
}

fn base_template<T: Into<BodyChild>>(children: Vec<T>) -> Html {
    let mut c: Vec<BodyChild> = Vec::new();

    c.push(header().into());
    c.extend(children.into_iter().map(|child| child.into()));
    c.push(footer().into());

    root(c)
}

fn read_random_line(file_path: &str) -> io::Result<String> {
    let file = File::open(file_path)?;
    let reader = BufReader::new(file);
    let lines: Result<Vec<String>, io::Error> = reader.lines().collect();
    let lines = lines?;
    if lines.is_empty() {
        return Err(io::Error::new(io::ErrorKind::InvalidData, "File is empty"));
    }
    let mut rng = rand::thread_rng();
    let random_index = rng.gen_range(0..lines.len());
    Ok(lines[random_index].clone())
}

pub fn read_file_to_string(file_path: &str) -> io::Result<String> {
    let mut file = File::open(file_path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    Ok(contents)
}

fn header() -> Header {
    let motd = read_file_to_string("motd.txt").unwrap_or("".to_owned());

    Header::builder()
        .division(|div| {
            div.class("banner")
                .anchor(|anchor| {
                    anchor.href("/").image(|image| {
                        image
                            .src(format!(
                                "/res/banners/{}.png",
                                rand::thread_rng().gen_range(1..4)
                            ))
                            .alt("Bannière ctrlist")
                            .class("banner-img")
                    })
                })
                .paragraph(|p| p.class("motd").text(format!("{}", motd)))
        })
        .navigation(|nav| {
            nav.unordered_list(|list| {
                list.list_item(|item| item.anchor(|a| a.href("/").text("discussions")))
                    .list_item(|item| item.anchor(|a| a.href("/about").text("à propos")))
                    .list_item(|item| item.anchor(|a| a.href("/links").text("annuaire")))
            })
        })
        .build()
}

fn footer() -> Footer {
    let quote = read_random_line("quotes.txt").unwrap_or("".to_owned());
    Footer::builder()
        .navigation(|nav| {
            nav.unordered_list(|list| {
                list.list_item(|item| item.anchor(|a| a.href("/conduct").text("code de conduite")))
                    .list_item(|item| item.anchor(|a| a.href("/login").text("connexion")))
                    .list_item(|item| item.anchor(|a| a.href("/admin").text("administration")))
            })
        })
        .block_quote(|b| b.paragraph(|p| p.text(quote)))
        .build()
}

fn thread_list<'a, T: Iterator<Item = &'a Post>>(conn: &Connection, threads: T) -> UnorderedList {
    UnorderedList::builder()
        .class("thread-list")
        .extend(threads.map(|data| thread_list_item(&conn, data)))
        .build()
}

fn thread_list_item(conn: &Connection, data: &Post) -> ListItem {
    let body = encode(
        data.body.as_bytes(),
        &EncodeType::NamedOrHex,
        &CharacterSet::Html,
    )
    .to_string()
    .unwrap();
    let nick = encode(
        data.nick.as_bytes(),
        &EncodeType::NamedOrHex,
        &CharacterSet::HtmlAndNonASCII,
    )
    .to_string()
    .unwrap();

    let color = string_to_color(data.author.clone().as_str());

    let reply_count = get_posts_in_thread(conn, data.id).unwrap_or(vec![]).len();

    ListItem::builder()
        .anchor(|a| {
            a.href(format!("/posts/{}?r={}", data.id, reply_count))
                .text(format!("#{} {}", data.id, get_summary(body.as_str(), 64)))
        })
        .span(|span| {
            span.span(|span| {
                span.class("nick")
                    .style(format!("color: {};", color))
                    .text(format!("{}", nick))
            })
            .span(|span| {
                span.class("time")
                    .text(format!("{}", timestamp_to_string(data.time)))
            })
        })
        .build()
}

fn get_summary(input: &str, max_length: usize) -> String {
    let first_newline_index = input.find(|c| c == '\n' || c == '.').unwrap_or(input.len());
    let first_line = &input[..first_newline_index];
    let shortened_line = if first_line.len() > max_length {
        format!("{}...", &first_line[..max_length].trim_end().to_owned())
    } else {
        first_line.to_owned()
    };

    shortened_line
}

fn post_form(action: String) -> Form {
    Form::builder()
        .id("post")
        .action(action)
        .method("post")
        .enctype("multipart/form-data")
        .class("post-form")
        .division(|div| {
            div.input(|input| input.type_("text").name("nick").placeholder("pseudo"))
                .text_area(|input| {
                    input
                        .name("message")
                        .placeholder("message")
                        .max_length(6850)
                        .required(true)
                })
                .input(|input| input.type_("file").name("upload"))
        })
        .input(|input| input.type_("submit").aria_label("Envoyer").value("➤"))
        .build()
}

fn login_form(action: String) -> Form {
    Form::builder()
        .id("post")
        .action(action)
        .method("post")
        .class("login-form")
        .input(|input| input.type_("text").name("username").placeholder("username"))
        .input(|input| {
            input
                .type_("password")
                .name("password")
                .placeholder("password")
        })
        .input(|input| input.type_("submit").value("send"))
        .build()
}

fn timestamp_to_string(timestamp: i64) -> String {
    DateTime::from_timestamp(timestamp, 0)
        .expect("invalid timestamp")
        .format("%d/%m/%Y %H:%M")
        .to_string()
}

fn post_list<'a, T: Iterator<Item = &'a Post>>(
    posts: T,
    conn: &Connection,
    priv_level: StaffPerms,
) -> Division {
    Division::builder()
        .class("post-list")
        .extend(posts.map(|data| post(data, conn, priv_level.clone())))
        .build()
}

fn string_to_color(input: &str) -> String {
    let mut hasher = Sha256::new();
    hasher.update(input);
    let result = hasher.finalize();

    let h = (u16::from(result[8]) as f32 / 255.0) * 360.0;
    let s = 0.5 as f32;
    let l = 0.5 as f32;

    let hsl = Hsl::new(h, s, l);
    let rgb: Srgb = Srgb::from_color(hsl);

    let r = (rgb.red * 255.0).round() as u8;
    let g = (rgb.green * 255.0).round() as u8;
    let b = (rgb.blue * 255.0).round() as u8;

    format!("#{:02X}{:02X}{:02X}", r, g, b)
}

fn post(data: &Post, conn: &Connection, priv_level: StaffPerms) -> Article {
    let mut body = encode(
        data.body.as_bytes(),
        &EncodeType::NamedOrHex,
        &CharacterSet::HtmlAndNonASCII,
    )
    .to_string()
    .unwrap();
    let nick = encode(
        data.nick.as_bytes(),
        &EncodeType::NamedOrHex,
        &CharacterSet::HtmlAndNonASCII,
    )
    .to_string()
    .unwrap();

    let color = string_to_color(data.author.clone().as_str());

    body = format_paragraph(body.as_str());
    body = format_bold(body.as_str());
    body = format_quote(body.as_str());
    body = format_mentions(body.as_str(), conn);
    body = format_urls(body.as_str());

    let mentions = get_mentions_by_target(conn, data.id).unwrap_or(vec![]);
    let mut references: Vec<Post> = vec![];

    for mention in mentions {
        let post = get_post_by_id(conn, mention.post_id);
        let _ = post.map(|o| o.map(|p| references.push(p)));
    }

    let admin_controls: Option<Form> = match priv_level {
        StaffPerms::Admin | StaffPerms::Janitor => Some(
            Form::builder()
                .class("admin-form")
                .action("POST")
                .input(|input| {
                    input
                        .type_("hidden")
                        .name("id")
                        .value(format!("{}", data.id))
                })
                .input(|input| {
                    input
                        .type_("submit")
                        .formaction("/post/mod/delete")
                        .formmethod("POST")
                        .value("Delete")
                })
                .input(|input| {
                    input
                        .type_("submit")
                        .formaction("/post/mod/ban")
                        .formmethod("POST")
                        .value("Ban")
                })
                .input(|input| {
                    input
                        .type_("submit")
                        .formaction("/post/mod/purge")
                        .formmethod("POST")
                        .value("Purge")
                })
                .build(),
        ),
        _ => None,
    };

    let footer: Option<Footer> = match references.first() {
        Some(_) => Some(
            Footer::builder()
                .unordered_list(|ul| {
                    ul.class("mentions")
                        .extend(references.into_iter().map(|post| {
                            let parent = match post.parent {
                                Some(id) => format!("{id}"),
                                None => "".to_owned(),
                            };
                            ListItem::builder()
                                .anchor(|a| {
                                    a.href(format!(r#"/posts/{}#{}"#, parent, post.id))
                                        .text(format!("#{}", post.id))
                                })
                                .build()
                        }))
                })
                .build(),
        ),
        None => None,
    };

    Article::builder()
        .class("post")
        .id(format!("{}", data.id))
        .header(|header: &mut html::content::builders::HeaderBuilder| {
            header
                .anchor(|a| {
                    a.href(format!("#{}", data.id))
                        .span(|span| span.text(format!("#{}", data.id)).class("id"))
                })
                .span(|span| {
                    span.text(nick)
                        .class("nick")
                        .style(format!("color: {};", color))
                })
                .span(|span| span.text(timestamp_to_string(data.time)).class("time"))
        })
        .division(|div| {
            div.class("post-content")
                .extend(data.upload.into_iter().map(|upload_id| {
                    Division::builder()
                        .class("post-upload")
                        .figure(|fig| {
                            fig.anchor(|a| {
                                a.href(format!("/uploads/{}.avif", upload_id)).image(|img| {
                                    img.class("post-img")
                                        .src(format!("/uploads/{}.avif", upload_id))
                                })
                            })
                            .figure_caption(|cap| cap.text(format!("◩ {}.avif", upload_id)))
                        })
                        .build()
                }))
                .division(|div| {
                    div.class("post-text")
                        .text(body)
                })
        })
        .extend(footer.map(|list| list))
        .extend(admin_controls.map(|controls| controls))
        .build()
}

fn root<T: Into<BodyChild>>(children: Vec<T>) -> Html {
    Html::builder()
        .lang("fr")
        .head(|head| {
            head.title(|title| title.text("ctrlist.org"))
                // TODO : dynamic page title
                .link(|link| {
                    link.rel("icon")
                        .type_("image/svg+xml")
                        .href("/res/favicon.png")
                })
                .link(|link| {
                    link.href("/res/styles.css")
                        .rel("stylesheet")
                        .type_("text/css")
                })
                .meta(|meta| {
                    meta.name("viewport")
                        .content("width=device-width, initial-scale=1.0")
                })
        })
        .body(|body| body.extend(children))
        .build()
}
