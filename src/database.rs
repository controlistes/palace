use crate::{auth::User, parser::grab_mentions, CreatePostError, CreatePostRequest};
use axum_typed_multipart::TypedMultipart;
use chrono::prelude::*;
use image::io::Reader;
use password_auth::generate_hash;
use ravif::{Encoder, Img, RGBA8};
use rusqlite::{params, Connection};
use std::fs;
use std::io::Cursor;

#[derive(Debug)]
pub struct Post {
    pub id: i32,
    pub time: i64,
    pub nick: String,
    pub body: String,
    pub author: String,
    pub upload: Option<i64>,
    pub parent: Option<i32>,
}

pub struct Mention {
    pub post_id: i32,
    pub target_id: i32,
}

#[derive(Debug, PartialEq)]
pub struct UserBan {
    pub ip: String,
    pub reason: Option<String>,
}

pub fn init_db() -> Result<Connection, rusqlite::Error> {
    let conn = Connection::open("palace.db")?;

    // CREATING POSTS TABLE
    conn.execute(
        "CREATE TABLE IF NOT EXISTS posts (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            time INTEGER NOT NULL,
            nick TEXT NOT NULL,
            body TEXT NOT NULL,
            author TEXT NOT NULL,
            upload INTEGER,
            parent INTEGER,
            FOREIGN KEY (parent) REFERENCES posts(id) ON DELETE CASCADE
        )",
        [],
    )?;

    // CREATING MENTIONS TABLE
    conn.execute(
        "CREATE TABLE IF NOT EXISTS mentions (
            post_id INT NOT NULL,
            target_id INT NOT NULL,
            FOREIGN KEY (post_id) REFERENCES posts(id) ON DELETE CASCADE,
            FOREIGN KEY (target_id) REFERENCES posts(id) ON DELETE CASCADE
        )",
        [],
    )?;

    // CREATING (STAFF) USER TABLE
    conn.execute(
        "CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            username TEXT NOT NULL,
            password TEXT NOT NULL,
            level INTEGER NOT NULL
        )",
        [],
    )?;

    // CREATING BAN TABLE
    conn.execute(
        "CREATE TABLE IF NOT EXISTS bans (
            ip TEXT NOT NULL,
            reason TEXT
        )",
        [],
    )?;

    Ok(conn)
}

pub fn create_mention(
    conn: &Connection,
    post_id: i64,
    target_id: i32,
) -> Result<usize, rusqlite::Error> {
    conn.execute(
        "INSERT INTO mentions (post_id, target_id) VALUES (?1, ?2)",
        params![post_id, target_id],
    )
}

pub fn create_ban(
    conn: &Connection,
    ip: &str,
    reason: Option<String>,
) -> Result<usize, rusqlite::Error> {
    conn.execute(
        "INSERT INTO bans (ip, reason) VALUES (?1, ?2)",
        params![ip, reason],
    )
}

pub fn delete_ban(conn: &Connection, ip: String) -> Result<usize, rusqlite::Error> {
    conn.execute("DELETE FROM bans WHERE ip = ?", params![ip])
}

pub fn create_user(
    conn: &Connection,
    username: &str,
    password: &str,
    level: &i64,
) -> Result<usize, rusqlite::Error> {
    let password = generate_hash(password);
    conn.execute(
        "INSERT INTO users (username, password, level) VALUES (?1, ?2, ?3)",
        params![username, password, level],
    )
}

pub fn delete_user(conn: &Connection, id: i32) -> Result<usize, rusqlite::Error> {
    conn.execute("DELETE FROM users WHERE id = ?", params![id])
}

pub fn get_mentions_by_target(
    conn: &Connection,
    target: i32,
) -> Result<Vec<Mention>, rusqlite::Error> {
    let mut statement = conn.prepare("SELECT * FROM mentions WHERE target_id = ?")?;
    let p_iter = statement.query_map([target], |row| {
        Ok(Mention {
            post_id: row.get(0)?,
            target_id: row.get(1)?,
        })
    })?;

    let mut mentions = Vec::new();
    for mention in p_iter {
        mentions.push(mention?);
    }
    Ok(mentions)
}

const STALE_THREADS_TIME_THRESHOLD: i32 = 2592000; // 30 for testing, 2592000 for prod
const STALE_THREADS_OFFSET: i32 = 15;

fn cull_threads(conn: &mut Connection) -> Result<(), rusqlite::Error> {
    let tx = conn.transaction()?;

    tx.execute(format!("
        CREATE TEMP VIEW sel_thread AS
        SELECT thread.id, thread.upload
        FROM posts thread
        LEFT JOIN posts post ON post.parent = thread.id
        WHERE thread.parent IS NULL
        AND ((post.id IS NULL AND thread.time < strftime('%s', 'now') - {0}) OR post.time < strftime('%s', 'now') - {0})
        ORDER BY thread.time DESC
        LIMIT -1 OFFSET {1};", STALE_THREADS_TIME_THRESHOLD, STALE_THREADS_OFFSET).as_str(), [])?;

    let mut stmt = tx.prepare("
        SELECT t.upload
        FROM sel_thread t
        WHERE t.upload IS NOT NULL
        UNION
        SELECT p.upload
        FROM sel_thread t
        INNER JOIN posts p ON p.parent = t.id
        WHERE p.upload IS NOT NULL;")?;

    let imgs_to_delete: Vec<i64>;
    let rows = stmt.query_map([], |row| { row.get::<_, i64>(0) })?;
    imgs_to_delete = rows.filter_map(|row| row.ok()).collect();


    tx.execute("DROP VIEW sel_thread;", [])?;

    tx.execute("
        DELETE
        FROM posts
        WHERE id IN (
            SELECT p1.id
            FROM posts p1
            LEFT JOIN posts p2 ON p2.parent = p1.id
            WHERE p1.parent IS NULL
            AND ((p2.id IS NULL AND p1.time < strftime('%s', 'now') - ?1) OR p2.time < strftime('%s', 'now') - ?1)
            ORDER BY p1.time DESC
            LIMIT -1 OFFSET ?2
        );", params!(STALE_THREADS_TIME_THRESHOLD, STALE_THREADS_OFFSET)
    )?;

    stmt.finalize()?;

    let commit = tx.commit();

    // Delete images
    if commit.is_ok() {
        for img in imgs_to_delete {
            let file_path = format!("uploads/{}.avif", img);
            let _ = fs::remove_file(file_path);
        }
    };

    commit
}

pub fn create_post(
    conn: &mut Connection,
    ip: &str,
    data: TypedMultipart<CreatePostRequest>,
    parent: Option<i32>,
) -> Result<i64, CreatePostError> {
    let now = Utc::now().timestamp();

    let nick = Some(data.nick.trim())
        .filter(|nick| !nick.is_empty())
        .unwrap_or("anon");

    if data.message.trim().is_empty() == true {
        Err(CreatePostError::InvalidForm)?;
    }

    if data.message.len() > 7000 {
        Err(CreatePostError::InvalidForm)?
    }

    let mut query = "INSERT INTO posts (time, nick, body, author, upload, parent) VALUES (?1, ?2, ?3, ?4, ?5, ?6)";

    if let Some(parent) = parent {
        match get_post_by_id(conn, parent) {
            Ok(_) => {
                query = "INSERT INTO posts (time, nick, body, author, upload, parent)
                SELECT ?1, ?2, ?3, ?4, ?5, ?6
                WHERE (SELECT COUNT(*) FROM posts WHERE parent = ?6) < 128;"
            }
            Err(_) => Err(CreatePostError::MissingThread)?,
        }
    } else {
        let _cull = cull_threads(conn);
    }

    // println!("{:?}", data.upload);

    let upload_contents = data
        .upload
        .as_ref()
        .filter(|upload| upload.contents.len() > 0)
        .map(|upload| -> Result<_, CreatePostError> {
            let allowed_content_types = [Some("image/jpeg"), Some("image/png")];
            let content_type = &upload.metadata.content_type.as_deref();
            if !allowed_content_types.contains(content_type) {
                Err(CreatePostError::InvalidUpload)?;
            }

            Ok(&upload.contents)
        })
        .transpose()?;

    let upload_id = upload_contents
        .map(|contents| match save_upload(contents) {
            Ok(id) => Ok(id),
            Err(SaveUploadError::DecodeError) => Err(CreatePostError::InvalidUpload),
            Err(SaveUploadError::EncodeError) => Err(CreatePostError::Internal),
            Err(SaveUploadError::WriteError) => Err(CreatePostError::Internal),
        })
        .transpose()?;

    let tx = conn.transaction().unwrap();

    let exec = match tx.execute(
        query,
        params![now, nick, data.message, ip, upload_id, parent],
    ) {
        Ok(ins) => {
            if ins == 0 {
                return Err(CreatePostError::ReplyLimit);
            };
            Ok(tx.last_insert_rowid())
        }
        Err(_) => Err(CreatePostError::Internal),
    };

    let _ = match tx.commit() {
        Ok(_) => (),
        Err(_) => Err(CreatePostError::Internal)?,
    };

    match exec {
        Ok(post_id) => {
            let mentions: Vec<i32> = grab_mentions(data.message.as_str(), conn);
            for target_id in mentions {
                let _ = create_mention(conn, post_id, target_id);
            }

            Ok(post_id)
        }
        Err(_) => Err(CreatePostError::Internal)?,
    }
}

#[derive(Debug)]
enum SaveUploadError {
    DecodeError,
    EncodeError,
    WriteError,
}

fn save_upload(raw: &[u8]) -> Result<i64, SaveUploadError> {
    let reader = Reader::new(Cursor::new(raw))
        .with_guessed_format()
        .expect("Cursor io never fails");

    let image = match reader.decode() {
        Ok(image) => image,
        Err(_) => return Err(SaveUploadError::DecodeError),
    };

    let rgba_image = image.to_rgba8();

    let pixels: Vec<_> = rgba_image
        .pixels()
        .map(|p| RGBA8 {
            r: p.0[0],
            g: p.0[1],
            b: p.0[2],
            a: p.0[3],
        })
        .collect();

    let res = match Encoder::new()
        .with_quality(70.)
        .with_speed(8)
        .encode_rgba(Img::new(
            &pixels,
            image.width().try_into().unwrap(),
            image.height().try_into().unwrap(),
        )) {
        Ok(res) => res,
        Err(_) => return Err(SaveUploadError::EncodeError),
    };

    let upload_id = Utc::now().timestamp_millis();

    let path = format!("uploads/{}.avif", upload_id);
    match std::fs::write(path, res.avif_file) {
        Ok(_) => return Ok(upload_id),
        Err(_) => return Err(SaveUploadError::WriteError),
    }
}

pub fn populate_sample_data(conn: &Connection) -> Result<(), rusqlite::Error> {
    let now = Utc::now().timestamp();
    let mut statement = conn.prepare("SELECT COUNT(*) FROM posts")?;
    let count: i64 = statement.query_row([], |row| row.get(0)).unwrap_or(0);

    let password = generate_hash("admin");

    if count == 0 {
        conn.execute(
            "INSERT INTO posts (time, nick, body, author) VALUES (?1, ?2, ?3, ?4)",
            params![
                now,
                "auto",
                "Bienvenue dans Palace, un forum minimal codé en Rust. Ceci est un message automatique.",
                "123.123.123.123"
            ],
        )?;
        conn.execute(
            "INSERT INTO users (username, password, level) VALUES (?1, ?2, ?3)",
            params!["admin", password, 0],
        )?;
    }

    Ok(())
}

fn get_posts(conn: &Connection, query: String) -> Result<Vec<Post>, rusqlite::Error> {
    let mut statement = conn.prepare(query.as_str())?;
    let p_iter = statement.query_map([], |row| {
        Ok(Post {
            id: row.get(0)?,
            time: row.get(1)?,
            nick: row.get(2)?,
            body: row.get(3)?,
            author: row.get(4)?,
            upload: row.get(5)?,
            parent: row.get(6)?,
        })
    })?;

    let mut posts = Vec::new();
    for post in p_iter {
        posts.push(post?);
    }
    Ok(posts)
}

pub fn get_userbans(conn: &Connection) -> Result<Vec<UserBan>, rusqlite::Error> {
    let mut statement = conn.prepare("SELECT * FROM bans")?;
    let p_iter = statement.query_map([], |row| {
        Ok(UserBan {
            ip: row.get(0)?,
            reason: row.get(1)?,
        })
    })?;

    let mut bans = Vec::new();
    for ban in p_iter {
        bans.push(ban?);
    }
    Ok(bans)
}

pub fn get_users(conn: &Connection) -> Result<Vec<User>, rusqlite::Error> {
    let mut statement = conn.prepare("SELECT * FROM users")?;
    let p_iter = statement.query_map([], |row| {
        Ok(User {
            id: row.get(0)?,
            username: row.get(1)?,
            password: row.get(2)?,
            level: row.get(3)?,
        })
    })?;

    let mut users = Vec::new();
    for user in p_iter {
        users.push(user?);
    }
    Ok(users)
}

pub fn is_banned(conn: &Connection, ip: &str) -> Result<bool, rusqlite::Error> {
    let mut statement = conn.prepare("SELECT * FROM bans WHERE ip = ?")?;
    let p_iter = statement.query_map([ip], |row| {
        Ok(UserBan {
            ip: row.get(0)?,
            reason: row.get(1)?,
        })
    })?;

    let mut bans = Vec::new();
    for ban in p_iter {
        bans.push(ban?);
    }

    match bans.pop() {
        Some(_) => Ok(true),
        None => Ok(false),
    }
}

pub fn get_user_by_id(conn: &Connection, user_id: i32) -> Result<Option<User>, rusqlite::Error> {
    let mut statement = conn.prepare("SELECT * FROM users WHERE id = ?")?;
    let p_iter = statement.query_map([user_id], |row| {
        Ok(User {
            id: row.get(0)?,
            username: row.get(1)?,
            password: row.get(2)?,
            level: row.get(3)?,
        })
    })?;

    let mut users = Vec::new();
    for user in p_iter {
        users.push(user?);
    }
    Ok(users.pop())
}

pub fn get_user_by_name(
    conn: &Connection,
    username: &str,
) -> Result<Option<User>, rusqlite::Error> {
    let mut statement = conn.prepare("SELECT * FROM users WHERE username = ?")?;
    let p_iter = statement.query_map([username], |row| {
        Ok(User {
            id: row.get(0)?,
            username: row.get(1)?,
            password: row.get(2)?,
            level: row.get(3)?,
        })
    })?;

    let mut users = Vec::new();
    for user in p_iter {
        users.push(user?);
    }
    Ok(users.pop())
}

/*
fn get_names(conn: &Connection) -> Result<Vec<String>, rusqlite::Error> {
    let mut stmt = conn.prepare("SELECT name FROM people WHERE id = :id")?;
    let rows = stmt.query_map(&[(":id", &"one")], |row| row.get(0))?;

    let mut names = Vec::new();
    for name_result in rows {
        names.push(name_result?);
    }

    Ok(names)
}
*/

pub fn delete_post_by_id(conn: &mut Connection, post_id: i32) -> Result<(), rusqlite::Error> {
    let tx = conn.transaction()?;
    let imgs_to_delete: Vec<i64>;

    let mut stmt = tx.prepare(
        "WITH RECURSIVE to_delete(id, upload) AS (
            VALUES(?, NULL)
            UNION
            SELECT posts.id, posts.upload
            FROM posts
            JOIN to_delete ON posts.parent = to_delete.id
        )
        SELECT posts.upload
        FROM posts
        WHERE posts.id IN (SELECT id FROM to_delete);",
    )?;

    let rows = stmt.query_map(params![post_id], |row| row.get::<_, i64>(0))?;

    imgs_to_delete = rows.filter_map(|row| row.ok()).collect();

    stmt.finalize()?;

    tx.execute("DELETE FROM posts WHERE id = ?", params![post_id])?;

    let commit = tx.commit();

    // Delete images
    if commit.is_ok() {
        for img in imgs_to_delete {
            let file_path = format!("uploads/{}.avif", img);
            let _ = fs::remove_file(file_path);
        }
    };

    commit
}

pub fn delete_posts_by_author(conn: &mut Connection, author: &str) -> Result<(), rusqlite::Error> {
    let tx = conn.transaction()?;
    let imgs_to_delete: Vec<i64>;

    let mut stmt = tx.prepare(
        "WITH RECURSIVE to_delete(id, upload) AS (
            SELECT id, upload
            FROM posts
            WHERE author = ?
            UNION
            SELECT posts.id, posts.upload
            FROM posts
            JOIN to_delete ON posts.parent = to_delete.id
        )
        SELECT posts.upload
        FROM posts
        WHERE posts.id IN (SELECT id FROM to_delete);",
    )?;

    let rows = stmt.query_map(params![author], |row| row.get::<_, i64>(0))?;

    imgs_to_delete = rows.filter_map(|row| row.ok()).collect();

    stmt.finalize()?;

    tx.execute("DELETE FROM posts WHERE author = ?", params![author])?;

    let commit = tx.commit();

    // Delete images
    if commit.is_ok() {
        for img in imgs_to_delete {
            let file_path = format!("uploads/{}.avif", img);
            let _ = fs::remove_file(file_path);
        }
    };

    commit
}

pub fn get_post_by_id(conn: &Connection, post_id: i32) -> Result<Option<Post>, rusqlite::Error> {
    let mut statement = conn.prepare("SELECT * FROM posts WHERE id = ?")?;
    let p_iter = statement.query_map([post_id], |row| {
        Ok(Post {
            id: row.get(0)?,
            time: row.get(1)?,
            nick: row.get(2)?,
            body: row.get(3)?,
            author: row.get(4)?,
            upload: row.get(5)?,
            parent: row.get(6)?,
        })
    })?;

    let mut posts = Vec::new();
    for post in p_iter {
        posts.push(post?);
    }
    Ok(posts.pop())
}

pub fn get_posts_in_thread(
    conn: &Connection,
    thread_id: i32,
) -> Result<Vec<Post>, rusqlite::Error> {
    let mut statement = conn.prepare("SELECT * FROM posts WHERE parent = ?")?;
    let p_iter = statement.query_map([thread_id], |row| {
        Ok(Post {
            id: row.get(0)?,
            time: row.get(1)?,
            nick: row.get(2)?,
            body: row.get(3)?,
            author: row.get(4)?,
            upload: row.get(5)?,
            parent: row.get(6)?,
        })
    })?;

    let mut posts = Vec::new();
    for post in p_iter {
        posts.push(post?);
    }
    Ok(posts)
}

pub fn get_main_threads(conn: &Connection) -> Result<Vec<Post>, rusqlite::Error> {
    get_posts(
        conn,
        format!("SELECT * FROM posts WHERE parent IS NULL ORDER BY id DESC LIMIT {}", STALE_THREADS_OFFSET + 1),
    )
}
