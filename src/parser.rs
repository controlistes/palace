use nom::{
    bytes::complete::{tag, take_until, take_while1},
    combinator::recognize,
    sequence::{terminated, tuple},
    IResult,
};
use rusqlite::Connection;
use std::str;

use crate::database::{get_post_by_id, Post};

fn is_url_char(c: char) -> bool {
    match c {
        '.' | '/' | ':' | '-' | '_' | '?' | '#' | '=' => true,
        _ => c.is_alphanumeric(),
    }
}

fn url_parser(input: &str) -> IResult<&str, &str> {
    let (input, url) = recognize(tuple((
        take_while1(|c: char| c.is_alphabetic()),
        tag("://"),
        take_while1(is_url_char),
    )))(input)?;

    Ok((input, url))
}

fn is_digit(c: char) -> bool {
    c.is_digit(10)
}

fn mention_parser(input: &str) -> IResult<&str, &str> {
    let (input, mention) = recognize(tuple((tag("#"), take_while1(is_digit))))(input)?;

    Ok((input, mention))
}

fn bold_parser(input: &str) -> IResult<&str, &str> {
    let (input, bold) = recognize(tuple((tag("*"), take_until("*"), tag("*"))))(input)?;

    Ok((input, bold))
}

fn quote_parser(input: &str) -> IResult<&str, &str> {
    let (input, quote) = recognize(tuple((tag("\""), take_until("\""), tag("\""))))(input)?;

    Ok((input, quote))
}

fn paragraph_parser(input: &str) -> IResult<&str, &str> {
    let (input, paragraph) = terminated(take_until("\n\r"), tag("\n\r"))(input)?;
    Ok((input, paragraph.trim()))
}

pub fn format_paragraph(input: &str) -> String {
    let mut output = String::new();
    let mut remaining_input = input;

    while !remaining_input.is_empty() {
        match paragraph_parser(remaining_input) {
            Ok((next_input, paragraph_text)) => {
                output.push_str(&format!("<p>{}</p>", paragraph_text));
                remaining_input = next_input;
            }
            Err(_) => {
                // Handle the last paragraph or single line paragraphs
                if !remaining_input.trim().is_empty() {
                    output.push_str(&format!("<p>{}</p>", remaining_input.trim()));
                }
                break;
            }
        }
    }

    output
}

pub fn format_quote(input: &str) -> String {
    let mut output = String::new();
    let mut remaining_input = input;

    while !remaining_input.is_empty() {
        match quote_parser(remaining_input) {
            Ok((next_input, quote_text)) => {
                output.push_str(&format!("<i>{}</i>", quote_text));
                remaining_input = next_input;
            }
            Err(_) => {
                let mut chars = remaining_input.chars();
                output.push(chars.next().unwrap());
                remaining_input = chars.as_str();
            }
        }
    }

    output
}

pub fn format_bold(input: &str) -> String {
    let mut output = String::new();
    let mut remaining_input = input;

    while !remaining_input.is_empty() {
        match bold_parser(remaining_input) {
            Ok((next_input, bold_text)) => {
                output.push_str(&format!("<b>{}</b>", bold_text));
                remaining_input = next_input;
            }
            Err(_) => {
                let mut chars = remaining_input.chars();
                output.push(chars.next().unwrap());
                remaining_input = chars.as_str();
            }
        }
    }

    output
}

pub fn format_urls(input: &str) -> String {
    let mut output = String::new();
    let mut remaining_input = input;

    while !remaining_input.is_empty() {
        match url_parser(remaining_input) {
            Ok((next_input, url)) => {
                output.push_str(&format!(r#"<a href="{}">{}</a>"#, url, url));
                remaining_input = next_input;
            }
            Err(_) => {
                let mut chars = remaining_input.chars();
                output.push(chars.next().unwrap());
                remaining_input = chars.as_str();
            }
        }
    }

    output
}

pub fn format_mentions(input: &str, conn: &Connection) -> String {
    let mut output = String::new();
    let mut remaining_input = input;

    while !remaining_input.is_empty() {
        match mention_parser(remaining_input) {
            Ok((next_input, mention)) => {
                let split = mention.trim_start_matches('#');
                let id: i32 = split.parse().unwrap_or(0);
                let post: Option<Post> = get_post_by_id(conn, id).unwrap_or(None);
                match post {
                    Some(post) => {
                        let thread_id = post.parent.unwrap_or(post.id);
                        output.push_str(&format!(
                            r#"<a href="/posts/{thread_id}#{split}" class="mention">{mention}</a>"#
                        ));
                    }
                    None => {
                        output.push_str(&format!(
                            r#"<a href="{mention}" class="mention none">{mention}</a>"#
                        ));
                    }
                }
                remaining_input = next_input;
            }
            Err(_) => {
                let mut chars = remaining_input.chars();
                output.push(chars.next().unwrap());
                remaining_input = chars.as_str();
            }
        }
    }

    output
}

pub fn grab_mentions(input: &str, conn: &Connection) -> Vec<i32> {
    let mut output: Vec<i32> = vec![];
    let mut remaining_input = input;

    while !remaining_input.is_empty() {
        match mention_parser(remaining_input) {
            Ok((next_input, mention)) => {
                let split = mention.trim_start_matches('#');
                let id: i32 = split.parse().unwrap_or(0);
                let post: Option<Post> = get_post_by_id(conn, id).unwrap_or(None);
                post.map(|p| {
                    if !output.contains(&p.id) {
                        output.push(p.id)
                    }
                });

                remaining_input = next_input;
            }
            Err(_) => {
                let mut chars = remaining_input.chars();
                let _ = chars.next().unwrap();
                remaining_input = chars.as_str();
            }
        }
    }

    output
}
